#!/usr/bin/env python

# Plugin to copy the x, y, width, and height of the selection bounds to
# the clipboard

from gimpfu import *
import pyperclip

def copy_selection_bounds(img):
    if img == None:
        return
    non_empty, x1, y1, x2, y2 = pdb.gimp_selection_bounds(img)
    if not non_empty:
        return

    x = min(x1, x2)
    y = min(y1, y2)
    width = abs(x2 - x1)
    height = abs(y2 - y1)

    pyperclip.copy("%d %d %d %d" % (x, y, width, height))

register(
    "python_fu_copy_selection_bounds",
    "Copies the bounds of the current selection to the clipboard as 'x y width height'",
    "Copies the bounds of the current selection to the clipboard as 'x y width height'",
    "ntfwc",
    "ntfwc",
    "2018",
    "Copy Selection Bounds",
    "*",
    [
        (PF_IMAGE, "image", "Input image", None),
    ],
    [],
    copy_selection_bounds, menu="<Image>/Select")

main()
