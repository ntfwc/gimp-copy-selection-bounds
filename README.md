# Description #

This is a plugin that allows you to easily copy the bounds of a selection box in Gimp. The bounds are copied in the form "x y width height", for example: 172 136 170 118

I made this to help me map out a spritesheet. I would recommend adding a key mapping if you use it for that purpose.

# Dependencies #

* The pyperclip python module
